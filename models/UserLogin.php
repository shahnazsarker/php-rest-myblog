<?php
class UserLogin
{
    //DB stuff
    private $conn;
    private $table = 'users';

    // Post Properties
    public $user_email;
    public $user_pwd;

    // Constructor with DB
    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function  emptyInputSignUp($user_email, $user_pwd){

        if (empty($user_email) || empty($user_pwd) ){
            $result =true;
        }
        else{
            $result=false;
        }
        return $result;
    }

    public function invalidEmail($user_email)
    {
        if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            $result = true;
        } else {
            $result = false;
        }
        return $result;

    }
    public function checkPassword($user_pwd, $hashedPwd)
    {
        $checkPwd = password_verify($user_pwd, $hashedPwd);


       return $checkPwd;
    }



//Register user
    public function login()
    {

        // check if input fields are empty

        $isEmptyInput = $this->emptyInputSignUp($this->user_email, $this->user_pwd);

        if( $isEmptyInput ){
            return "emptyInput";
        }

        // check if email is valid
        $isEmailVerified = $this->invalidEmail($this->user_email);

        if($isEmailVerified ){
            return "invalidEmail";
        }
        // check user
        $checkUser = ' SELECT * FROM ' . $this->table . ' WHERE user_email= ? ';

        //prepare statement for existing user
        $checkStmt = $this->conn->prepare($checkUser);

        if ($checkStmt->execute([$this->user_email])) {
            $dbUser = $checkStmt->fetch(PDO::FETCH_ASSOC);
            if ( $this->checkPassword($this->user_pwd, $dbUser["user_pwd"]) ){
                return 'successful';
            }
            else{
                return 'incorrect';

            }

        }
        else {

            //print error if something goes wrong
            printf("Error: %s.\n", $checkStmt->error);
            return false;
        }


    }
}