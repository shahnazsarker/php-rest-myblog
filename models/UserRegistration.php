<?php
class UserRegistration
{
    //DB stuff
    private $conn;
    private $table = 'users';

    // Post Properties
    public $user_id;
    public $user_name;
    public $user_email;
    public $user_uid;
    public $user_pwd;

    // Constructor with DB
    public function __construct($db)
    {
        $this->conn = $db;
    }
    //get user
    // Get Posts
    public function read()
    {
        // Create query
        $query = 'SELECT user_id, user_name, user_email, user_uid, user_pwd FROM ' . $this->table . ' ORDER BY user_id DESC';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Execute query
        $stmt->execute();

        return $stmt;
    }
    public function  emptyInputSignUp($user_name, $user_email, $user_uid, $user_pwd){

        if (empty($user_name) || empty($user_email) || empty($user_uid) || empty($user_pwd) ){
            $result =true;
        }
        else{
            $result=false;
        }
        return $result;
    }

    public function invalidEmail($user_email)
    {
        if (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            $result = true;
        } else {
            $result = false;
        }
        return $result;

    }
    function invalidUID($user_uid){

        if(!preg_match("/^[a-zA-z0-9]*$/", $user_uid)){
            $result =true;
        }
        else{
            $result=false;
        }
        return $result;
    }
    public function hashPassword($user_pwd)
    {
        $hashedPwd = password_hash($user_pwd, PASSWORD_DEFAULT);
        return $hashedPwd;
    }



//Register user
    public function register()
    {

        // check if input fields are empty

        $isEmptyInput = $this->emptyInputSignUp($this->user_name, $this->user_email, $this->user_uid, $this->user_pwd);

        if( $isEmptyInput ){
            return "emptyInput";
        }
        // check if email is valid
        $isUIDVerified = $this->invalidUID($this->user_uid);

        if($isUIDVerified ){
            return "invalidUID";
        }
        // check if email is valid
        $isEmailVerified = $this->invalidEmail($this->user_email);

        if($isEmailVerified ){
            return "invalidEmail";
        }
        // check user
        $checkUser = ' SELECT * FROM ' . $this->table . ' WHERE user_email= ? ';

        //prepare statement for existing user
        $checkStmt = $this->conn->prepare($checkUser);

        if ($checkStmt->execute([$this->user_email])) {
            $dbUser = $checkStmt->fetch(PDO::FETCH_ASSOC);
            if ($dbUser['user_email'] == $this->user_email){
                printf($checkStmt->error, "Sorry email exists");
                return "userExist";
            }
            else{
                //create query
                $query = 'INSERT INTO ' . $this->table . ' SET  user_name = :user_name, user_email = :user_email, user_uid = :user_uid, user_pwd = :user_pwd';

                // prepare register statement
                $regStmt = $this->conn->prepare($query);

                // Clean data
                $this->user_name = htmlspecialchars(strip_tags($this->user_name));
                $this->user_email = htmlspecialchars(strip_tags($this->user_email));
                $this->user_uid = htmlspecialchars(strip_tags($this->user_uid));
                $this->user_pwd = htmlspecialchars(strip_tags($this->user_pwd));

                // bind data for register statement
                $regStmt->bindParam(':user_name', $this->user_name);
                $regStmt->bindParam(':user_email', $this->user_email);
                $regStmt->bindParam(':user_uid', $this->user_uid);
                $regStmt->bindParam(':user_pwd', $this->hashPassword($this->user_pwd));

                // execute query
                if ($regStmt->execute()) {
                    return "userRegistered";
                } else {
                    // print error if something goes wrong
                    printf("Error: %s.\n", $regStmt->error);

                    return false;
                }


            }

        }
        else {

         //print error if something goes wrong
           printf("Error: %s.\n", $checkStmt->error);
           return false;
       }


    }
}