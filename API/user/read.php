<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/UserRegistration.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate blog category
$user = new UserRegistration($db);

// Blog category query
$result = $user->read();
// Get row count
$num = $result->rowCount();

// Check if any categories
if($num > 0) {
    // category array
    //$user_arr = array();
    $user_arr['data'] = array();

    while($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $user_item = array(
            'user_id' => $user_id,
            'user_name' => $user_name,
            'user_email' => $user_email,
            'user_uid' => $user_uid,
            'user_pwd' => $user_pwd

        );

        // Push to "data"

        array_push($user_arr['data'], $user_item);
    }

    // Turn to JSON & output
    echo json_encode($user_arr);

} else {
    // No Posts
    echo json_encode(
        array('message' => 'No users Found')
    );
}