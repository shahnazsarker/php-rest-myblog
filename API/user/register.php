<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/UserRegistration.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate registration
$register = new UserRegistration($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

$register->user_name = $data->user_name;
$register->user_email = $data->user_email;
$register->user_uid = $data->user_uid;
$register->user_pwd = $data->user_pwd;
$message= $register->register();


//print ($message);
//die();
// Create post
if($message === "emptyInput"){
    echo json_encode(
        array('message' => 'Sorry empty input detected')
    );
}
else if($message === "userExist") {
    echo json_encode(
        array('message' => 'User Exists')
    );}
else if($message === "usernameExist") {
        echo json_encode(
            array('message' => 'Username Exists')
        );
} else if ($message === "userRegistered"){
    echo json_encode(
        array('message' => 'User Registered')
    );
}
else if ($message === "invalidUID"){
    echo json_encode(
        array('message' => "Invalid username")
    );
}
else if ($message === "invalidEmail"){
    echo json_encode(
        array('message' => "Email doesn't exist")
    );
}
else{
    echo json_encode(
        array('message' => 'User not Registered')
    );
}
