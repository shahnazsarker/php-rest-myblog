<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/UserLogin.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate registration
$login = new UserLogin($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));


$login->user_email = $data->user_email;

$login->user_pwd = $data->user_pwd;
$message= $login->login();


//print ($message);
//die();
// Create post
if($message === "emptyInput"){
    echo json_encode(
        array('message' => 'Sorry empty input detected')
    );
}
else if($message === "userExist") {
    echo json_encode(
        array('message' => 'User Exists')
    );}

else if ($message === "invalidEmail"){
    echo json_encode(
        array('message' => "Email doesn't exist")
    );
}
else if ($message === "incorrect"){
    echo json_encode(
        array('message' => "Incorrect Input")
    );
}
else if ($message === "successful"){
    echo json_encode(
        array('message' => "Successfully logged In")
    );
}
else{
    echo json_encode(
        array('message' => 'not logged in yet')
    );
}