<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Category.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate category
$post = new Category($db);

// Get raw posted data
$data = json_decode(file_get_contents("php://input"));

$post->id = $data->id;
$post->name = $data->name;

// Create Category
if($post->create()) {
    echo json_encode(
        array('message' => 'category made')
    );
} else {
    echo json_encode(
        array('message' => 'category not made')
    );
}
