# PHP REST API
###### Simple blogging system with API.
### User:

* can create post
* can update post
* can delete post
* can read posts
* can view single post
* can create category 
* can update category 
* can delete category 
* can read categories 
* can view single category
* can register/create an account
* can login
      